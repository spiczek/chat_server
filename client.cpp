#include "client.h"
#include "server_controler.h"
#include "login_values.h"

Client::Client(int socket_descriptor, Server_model *model, QObject *parent) : QThread()
{
    this->socket_descriptor = socket_descriptor;
    this->model = model;
    this->id_code = -1;


    this->controler = parent;
}

Client::~Client()
{
    qDebug() << "deleting client" << this->socket_descriptor << endl;
    if (id_code == -1)
        emit clear_jar(socket_descriptor*(-1));
    else
        emit clear_jar(id_code);
}


void Client::run()
{
    qDebug() << "starting thread socket descriptor: " << socket_descriptor << endl;

    socket = new QTcpSocket();

    if(!socket->setSocketDescriptor(this->socket_descriptor))
    {
        qDebug() << "couldn't set socket" << endl;
        return;
    }


    connect(socket, SIGNAL(readyRead()), this, SLOT(startRead()), Qt::DirectConnection);
    connect(socket, SIGNAL(disconnected()), this, SLOT(disconnected()), Qt::DirectConnection);

    qDebug() << "socket descriptor: " << socket_descriptor << " connected" << endl;

    exec();
}

bool Client::write_message(QByteArray data)
{

    socket->write(data);
    socket->waitForBytesWritten(30000);
    //socket->flush();
    qDebug() << "_______sending____________________" << data << endl;
    //socket->flush();

    return socket->waitForBytesWritten(5000);
}

void Client::startRead()
{

    QByteArray array = socket->readAll();
    code = new Transmition_code(array);

    switch (code->get_function())
    {
        case 0:
            login(code->get_args());
        break;

        case 1:
            send(code->get_args());
        break;

        case 2:
            _register(code->get_args());
        break;

        case 3:
            _add(code->get_args());
        break;

        default:
            qDebug() << "wrong function name" << endl;
    }

    delete code;
}

void Client::login(QStringList args)
{
    Login_values *returned;

    if (args.size() != 2)
    {
        qDebug() << QString("Error: incorrect number of arguments, provided: %1 needed 2.").arg(args.size()) << endl;
        return;
    }

    returned = model->login_user(args.at(0), args.at(1));

    QString msg;

    if (!returned->get_error().isEmpty())
    {
        msg = code->error_encode(returned->get_error());
    }
    else
    {

    this->id_code = returned->id_code();

    qDebug() << "#######" << returned->get_name() << returned->get_surname() << returned->get_login() << endl;


    msg = code->encode(QString::number(this->id_code), returned->get_name(), returned->get_surname(), returned->get_login());

    }

    delete returned;
    socket->write(msg.toUtf8());

}


void Client::send(QStringList args)
{

        if (args.size() != 3)
        {
            qDebug() << QString("Error: incorrect number of arguments, provided: %1 needed 3.").arg(args.size()) << endl;
            return;
        }

            qDebug() << "preparing for send..." << endl;

        ((Server_controler*)this->controler)->send_to_thread(args.at(1).toInt(), code->encode(args.at(0), args.at(2)));

}

void Client::_register(QStringList args)
{
    if (args.size() != 4)
    {
        qDebug() << QString("Error: incorrect number of arguments, provided: %1 needed 4.").arg(args.size()) << endl;
        return;
    }

    QString error = model->register_user(args.at(0), args.at(1), args.at(2), args.at(3));


    QString msg;
    if (error.isEmpty())

        msg = code->encode();
    else
        msg = code->error_encode(error);

    socket->write(msg.toUtf8());

}

void Client::_add(QStringList args)
{
    Login_values *returned;

    if (args.size() != 2)
    {
        qDebug() << QString("Error: incorrect number of arguments, provided: %1 needed 2.").arg(args.size()) << endl;
        return;
    }

    returned = model->exist_user(args.at(0));

    QString msg;

    if (!returned->get_error().isEmpty())
    {
        msg = code->error_encode(returned->get_error());

    }
    else
    {
        msg = code->encode(args.at(0), returned->get_name(), returned->get_surname(), returned->get_login());
    }

    socket->write(msg.toUtf8());

}


void Client::disconnected()
{
    qDebug() << "socket descriptor: " << socket_descriptor << "disconnected";

    socket->deleteLater();
    exit(0);
}

