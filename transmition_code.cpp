#include "transmition_code.h"

const QString Transmition_code::ARGS_SEPARATOR = "<;>";
const QString Transmition_code::FUNCTION_SEPARATOR = "<:>";
const QString Transmition_code::ERROR = "1";
const QString Transmition_code::NO_ERROR = "0";

Transmition_code::Transmition_code(QByteArray array)
{

    QString data(array);
    QStringList parts = data.split(FUNCTION_SEPARATOR);

    _function = parts.at(0).toInt();
    qDebug() << "code:" << _function;
    _args = parts.at(1).split(ARGS_SEPARATOR);

}

Transmition_code::Transmition_code(int id)
{
    _function = id;
}

int Transmition_code::get_function()
{
    return _function;
}

QStringList Transmition_code::get_args()
{
    return _args;
}


QString Transmition_code::error_encode(QString error)
{
    //:1;
    QString part = QString("%1%2%3").arg(FUNCTION_SEPARATOR).arg(ERROR).arg(ARGS_SEPARATOR);

    switch (_function)
    {
        case LOGIN_FUNCTION:
            //0:1;error
            return QString("%1%2%3").arg(LOGIN_FUNCTION).arg(part).arg(error);
        break;

        case SEND_FUNCTION:
            return QString("%1%2%3").arg(SEND_FUNCTION).arg(part).arg(error);
        break;

        case REGISTER_FUNCTION:
            return QString("%1%2%3").arg(REGISTER_FUNCTION).arg(part).arg(error);
        break;

        case ADD_FUNCTION:
            return QString("%1%2%3").arg(ADD_FUNCTION).arg(part).arg(error);
        break;

        default:
            return "";
    }


}

QString Transmition_code::encode(QString v1, QString v2, QString v3, QString v4)
{
    //v1;v2;v3;v4
    QString args = QString("%1%5%2%5%3%5%4").arg(v1).arg(v2).arg(v3).arg(v4).arg(ARGS_SEPARATOR);
    //:0;
    QString code0 = QString("%1%2%3").arg(FUNCTION_SEPARATOR).arg(NO_ERROR).arg(ARGS_SEPARATOR);

    switch (_function)
    {
        case LOGIN_FUNCTION:
            //0:0;v1;v2;v3;v4
        return QString("%1%2%3").arg(LOGIN_FUNCTION).arg(code0).arg(args);
        break;

        case SEND_FUNCTION:
            //send(code->get_args());
        break;

        case REGISTER_FUNCTION:
            //2::args
            return QString("%1%2%3").arg(REGISTER_FUNCTION).arg(FUNCTION_SEPARATOR).arg(args);
        break;

        case ADD_FUNCTION:
            return QString("%1%2%3").arg(ADD_FUNCTION).arg(code0).arg(args);
        break;

        default:
            return "";
    }

    return "";
}

QString Transmition_code::encode(QString v1, QString v2, QString v3)
{
    //1::v1;v2;v3
    return QString("%1%2%3%4%5%4%6").arg(SEND_FUNCTION).arg(FUNCTION_SEPARATOR).arg(v1).arg(ARGS_SEPARATOR).arg(v2).arg(v3);

}

QString Transmition_code::encode(QString v1, QString v2)
{
    if (_function == 1)
    {
        //1:0
        QString part1 = QString("%1%2%3").arg(SEND_FUNCTION).arg(FUNCTION_SEPARATOR).arg(NO_ERROR);
        //;v1;v2*
        QString part2 = QString("%1%2%1%3*").arg(ARGS_SEPARATOR).arg(v1).arg(v2);
        //1:0;%1;%2*
        return QString("%1%2").arg(part1).arg(part2);
    }
    else if (_function == 0)
    {
        //0::v1;v2
        return QString("%1%2%3%4%5").arg(LOGIN_FUNCTION).arg(FUNCTION_SEPARATOR).arg(v1).arg(ARGS_SEPARATOR).arg(v2);
    }

    return "";
}

QString Transmition_code::encode(QString v1)
{
    //3::%1;
    return QString("%1%2%3%4").arg(ADD_FUNCTION).arg(FUNCTION_SEPARATOR).arg(v1).arg(ARGS_SEPARATOR);

}

QString Transmition_code::encode()
{
    //2:0;
    return QString("%1%2%3%4").arg(REGISTER_FUNCTION).arg(FUNCTION_SEPARATOR).arg(NO_ERROR).arg(ARGS_SEPARATOR);
}





