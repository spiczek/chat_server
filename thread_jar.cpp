#include "thread_jar.h"

Thread_jar::Thread_jar()
{
    _threads = new QList<Client*>();
    qDebug() << "created jar" << endl;
}

Thread_jar::~Thread_jar()
{
    delete _threads;
}

void Thread_jar::add(Client *client)
{
    _threads->push_back(client);
    qDebug() << "added client id: " << client->id_code << "jar size: " << _threads->size() << endl;

}

void Thread_jar::remove(int id)
{
        if (id > 0)
        {
            for (int i = 0; i < _threads->size(); i++)
            {
                if (_threads->at(i)->id_code == id)
                {
                    _threads->removeAt(i);
                    qDebug() << "removed id: " << id << "from jar "
                             << "current size " << _threads->size() << endl;
                    return;
                }
            }
        }
        else
        {
            int to_find = id*(-1);
            for (int i = 0; i < _threads->size(); i++)
            {
                if (_threads->at(i)->socket_descriptor == to_find)
                {
                    _threads->removeAt(i);
                    qDebug() << "jar: removed " << id << endl;
                    qDebug() << "size " << _threads->size() << endl;
                    return;
                }
            }
        }
}


bool Thread_jar::send(int id, QString data)
{
    qDebug() << "sending message form id: " << data.at(4) << data.at(5) << "to id " << id << endl;

    for (int i = 0; i < _threads->size(); i++)
    {
        if (_threads->at(i)->id_code == id)
        {
            return _threads->at(i)->write_message(data.toUtf8());
        }
    }

    return 0;
}
