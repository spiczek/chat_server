#ifndef TRANSMITION_CODE_H
#define TRANSMITION_CODE_H

#include <QDebug>
#include <QString>
#include <QByteArray>
#include <QStringList>


/*! A class that provides transmition codes patterns. */
class Transmition_code
{
public:

    //! A constructor \param array data to decode or encode.
    Transmition_code(QByteArray array);
    //! A constructor \param id function code.
    Transmition_code(int id);

    //! A function that returns function code. \return function code
    int get_function();
    //! A function that returns arguments list. \return arguments list
    QStringList get_args();

    //! A function that encodes an error. \return encoded error
    QString error_encode(QString error);

    /*! A function that encodes provided paremeters.
        \param v1 parameter 1
        \param v2 parameter 2
        \param v3 parameter 3
        \param v4 parameter 4
        \return encoded parameters
    */
    QString encode(QString v1, QString v2, QString v3, QString v4);
    QString encode(QString v1, QString v2, QString v3);
    QString encode(QString v1, QString v2);
    QString encode(QString v1);
    QString encode();

private:
    //! A static variable that stores login code.
    static const int LOGIN_FUNCTION = 0;
    //! A static variable that stores send code.
    static const int SEND_FUNCTION = 1;
    //! A static variable that stores register code.
    static const int REGISTER_FUNCTION = 2;
    //! A static variable that stores add code.
    static const int ADD_FUNCTION = 3;
    //! A static variable that stores arguments separator code.
    static const QString ARGS_SEPARATOR;
    //! A static variable that stores function separator code.
    static const QString FUNCTION_SEPARATOR;
    //! A static variable that stores error code.
    static const QString ERROR;
    //! A static variable that stores no error code.
    static const QString NO_ERROR;
    //! A variable that stores current function code.
    int _function;
    //! A variable that stores current arguments list.
    QStringList _args;


};



#endif // TRANSMITION_CODE_H
