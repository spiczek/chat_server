#ifndef SERVER_CONTROLER_H
#define SERVER_CONTROLER_H

#include <QTcpServer>
#include "server_model.h"
#include "thread_jar.h"

class Server_controler: public QTcpServer
{

Q_OBJECT

public:

    Server_controler();
    ~Server_controler();
    bool send_to_thread(int id, QString data);

private slots:
    void clear_jar(int id);

private:
    Server_model *model;
    Thread_jar *thread_jar;

protected:
    void incomingConnection (int socketDescriptor);


};

#endif // SERVER_CONTROLER_H
