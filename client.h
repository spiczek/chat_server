#ifndef CLIENT_H
#define CLIENT_H

//#include <QObject>
//#include <QTcpSocket>
//#include <QDebug>
//#include <QByteArray>
//#include <QStringList>
#include "server_model.h"
#include "transmition_code.h"



class Client : public QThread
{
    Q_OBJECT

public:
    Client(int socket_descriptor, Server_model *model, QObject *parent);
    ~Client();
    void run();
    bool write_message(QByteArray data);
    int id_code;
    int socket_descriptor;



private:
    Server_model *model;
    QObject *controler;
    QTcpSocket *socket;
    Transmition_code *code;

    void _register(QStringList args);
    void _add(QStringList args);
    void send(QStringList args);
    void login(QStringList args);



private slots:
    void startRead();
    void disconnected();

signals:
    void clear_jar(int id);





};

#endif // CLIENT_H
