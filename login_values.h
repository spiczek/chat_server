#ifndef RETURNED_VALUE_H
#define RETURNED_VALUE_H

#include <QString>

class Login_values
{
public:
    Login_values(QString);
    Login_values(int id, QString name, QString surname, QString login);
    QString get_error();
    int id_code() { return this->id; }
    QString get_name() { return this->name; }
    QString get_surname() { return this->surname; }
    QString get_login() { return this->login; }


private:
    QString error;
    int id;
    QString name;
    QString surname;
    QString login;

    
};

#endif // RETURNED_VALUE_H
