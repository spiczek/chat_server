#ifndef SERVER_MODEL_H
#define SERVER_MODEL_H

#include <QString>
#include <QtSql>
#include <QTcpSocket>
#include "login_values.h"

//! A class that makes operations on a database
class Server_model
{

public:
    //! A constructor
    Server_model();
    //! A desturctor
    ~Server_model();

    /*! A function that add new user to database
      \param name a user name
      \param surname a user surname
      \param nick a user login
      \param user_password a user password
    */
    QString register_user(QString name, QString surname, QString nick, QString user_password);
    /*! A function that logins a user
      \param nick a user login
      \param user_password a user password
    */
    Login_values* login_user(QString nick, QString user_password);
    /*! A function that checks if user exists
      \param id a user id number
    */
    Login_values* exist_user(QString id);

private:
    QTcpSocket client; //! client socket
    QString login;
    QString password;
    QSqlDatabase db; //! database object
    
};

#endif // SERVER_MODEL_H
