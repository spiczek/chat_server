#include "server_controler.h"


Server_controler::Server_controler(): QTcpServer()
{

    this->model = new Server_model();
    this->thread_jar = new Thread_jar();



        if (!this->listen(QHostAddress::Any, 8888))//slucham nowych polaczen
        {
            qDebug() << "Server_controler could not start." << endl;
        }
        else
        {
            qDebug() << "Server_controler has started." << endl;
        }
}

Server_controler::~Server_controler()
{
    //Server_controler->close();
    delete model;
    delete thread_jar;
}

void Server_controler::incomingConnection (int socketDescriptor)
{
        Client *thread = new Client(socketDescriptor, model, this);

        connect(thread, SIGNAL(clear_jar(int)), this, SLOT(clear_jar(int)));
        connect(thread, SIGNAL(finished()), thread, SLOT(deleteLater()));

        thread->start();

        thread_jar->add(thread);
}

bool Server_controler::send_to_thread(int id, QString data)
{
    qDebug() << "searching thread" << endl;

    return this->thread_jar->send(id, data);
}

void Server_controler::clear_jar(int id)
{
        thread_jar->remove(id);
}
