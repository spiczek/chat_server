#ifndef THREAD_JAR_H
#define THREAD_JAR_H


//#include <QList>
//#include <QDebug>
#include "client.h"

/*! A class that gathers currently running threads. */
class Thread_jar
{

public:
    //! A constructor
    Thread_jar();
    //! A destructor
    ~Thread_jar();
    /*! A function that adds new thread
      \param client a class that controls user activities
    */
    void add(Client *client);
    /*! A function that sends message to a user in different thread
      \param id a reciver id
      \param data massage to be sent
    */
    bool send(int id, QString data);
    /*! A function that sends message to a user in different thread
      \param id user id to be deleted
    */
    void remove(int id);

private:

    QList<Client*> *_threads; //! A list of threads


    
};

#endif // THREAD_JAR_H
