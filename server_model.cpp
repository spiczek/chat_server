#include "server_model.h"

Server_model::Server_model()
{

    login = "root";
    password = "root";

    db = QSqlDatabase::addDatabase("QMYSQL");

    db.setHostName("localhost");
    db.setDatabaseName("chat");
    db.setUserName(login);
    db.setPassword(password);


    //db = QSqlDatabase::addDatabase("QSQLITE");

    //db.setDatabaseName("/home/piotr/chat.db");



}

Server_model::~Server_model()
{
    //db.removeDatabase("chat");
    //QSqlDatabase::removeDatabase("QMYSQL");
}

QString Server_model::register_user(QString name, QString surname, QString nick, QString user_password)
{
        QSqlQuery query;

        try
        {
            if (!db.open())
            {
                db.close();
                return db.lastError().text();
            }

                query.prepare("SELECT * FROM user WHERE nick = ?");

                    query.bindValue(0, nick);

                        if (!query.exec())
                        {
                            db.close();
                            return query.lastError().text();
                        }

                        if (query.size() == 0)
                        {
                                query.clear();
                                query.prepare("INSERT INTO user VALUES('', ?, ?, ?, ?)");

                                    query.bindValue(0, name);
                                    query.bindValue(1, surname);
                                    query.bindValue(2, nick);
                                    query.bindValue(3, user_password);

                                    if (!query.exec())
                                    {
                                        db.close();
                                        return query.lastError().text();
                                    }
                        }
                        else
                        {
                            db.close();
                            return "Login that you have provided already exists.";
                        }

        }
        catch(QSqlError e)
        {
            db.close();
            return e.driverText();
        }

        return "";
}

Login_values* Server_model::login_user(QString nick, QString user_password)
{

        QSqlQuery query;
        int id;
        QString login;
        QString name;
        QString surname;

        qDebug() << "**" << nick << "**" << "**" << user_password;
        qDebug() << nick.size() << user_password.size() << endl;

        try
        {
qDebug() << "_____________________________";
            if (!db.open())
            {
                db.close();
                return new Login_values(db.lastError().text());
            }

                query.prepare("SELECT * FROM user WHERE nick = ? AND password = ?");

                    query.bindValue(0, nick);
                    query.bindValue(1, user_password);

                    if (!query.exec())
                    {
                        db.close();
                        return new Login_values(query.lastError().text());
                    }

                    if (query.size() == 0)
                    {
                        db.close();
                        return new Login_values("Incorrect password or login.");
                    }

                    query.next();
                    id = query.value(0).toInt();
                    name = query.value(1).toString();
                    surname = query.value(2).toString();
                    login = query.value(3).toString();




        }
        catch(QSqlError e)
        {
            db.close();
            return new Login_values(e.driverText());
        }

        return new Login_values(id, name, surname, login);
}

Login_values* Server_model::exist_user(QString id)
{

        QSqlQuery query;

        QString login;
        QString name;
        QString surname;

        try
        {
            if (!db.open())
            {
                db.close();
                return new Login_values(db.lastError().text());
            }

                query.prepare("SELECT * FROM user WHERE id = ?");

                    query.bindValue(0, id.toInt());


                    if (!query.exec())
                    {
                        db.close();
                        return new Login_values(query.lastError().text());
                    }

                    if (query.size() == 0)
                    {
                        db.close();
                        return new Login_values("Id that you have provided doesen't exist.");
                    }

                    query.next();
                    id = query.value(0).toInt();
                    name = query.value(1).toString();
                    surname = query.value(2).toString();
                    login = query.value(3).toString();

        }
        catch(QSqlError e)
        {
            db.close();
            return new Login_values(e.driverText());
        }

        return new Login_values(id.toInt(), name, surname, login);
}



