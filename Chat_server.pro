#-------------------------------------------------
#
# Project created by QtCreator 2012-12-07T21:21:43
#
#-------------------------------------------------

QT       += core
QT       += network
QT       += gui
QT       += sql

TARGET = Chat_server
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    client.cpp \
    thread_jar.cpp \
    login_values.cpp \
    transmition_code.cpp \
    server_controler.cpp \
    server_model.cpp

HEADERS += \
    client.h \
    thread_jar.h \
    login_values.h \
    transmition_code.h \
    server_model.h \
    server_controler.h
