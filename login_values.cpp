#include "login_values.h"

Login_values::Login_values(QString error)
{
    this->error = error;
}

Login_values::Login_values(int id, QString name, QString surname, QString login)
{
    this->error = "";
    this->id = id;
    this->name = name;
    this->surname = surname;
    this->login = login;
}

QString Login_values::get_error()
{
    return this->error;
}
